package com.orchestrator.smartsheet;

import java.util.ArrayList;
import java.util.List;s
import java.util.Objects;

import com.orchestrator.model.TicketData;
import com.smartsheet.api.Smartsheet;
import com.smartsheet.api.SmartsheetException;
import com.smartsheet.api.SmartsheetFactory;
import com.smartsheet.api.models.Cell;
import com.smartsheet.api.models.Row;
import com.smartsheet.api.models.Sheet;

public class ReadData {

	
	
	public List<TicketData> read() throws SmartsheetException{
		// Initialize client
		String accessToken = "av34kpaaprfvn1h6kc6sciy8tk";

		// Load all smartsheet
		Smartsheet smartsheet = SmartsheetFactory.createDefaultClient(accessToken);

		// Our sheet id
		long sheetId = 3571018833913732L; 


		// Load the entire sheet
		Sheet sheet = smartsheet.sheetResources().getSheet(sheetId,null,null,null, null,null,null,null); 

		// Load the sheet row
		List<Row> L = sheet.getRows();
		
		
		List<TicketData> list = new ArrayList<TicketData>();
		
		for (Row row : L) {
			
			List<Cell> cellList = row.getCells();
			TicketData d = new TicketData();
			
			int count = 0;
			for (Cell cell : cellList) {

				String cellOutput = Objects.toString(cell.getValue());

				if (count == 0)
					d.setTicNo(cellOutput);
				if (count == 1)
					d.setType(cellOutput);
				if (count == 2)
					d.setSpoc(cellOutput);
				if (count == 3)
					d.setStatus(cellOutput);
				count++;;
			}

			if (d.getType()=="null"&&d.getTicNo()=="null"&&d.getSpoc()=="null"&&d.getStatus()=="null") {}     		  
			else list.add(d);
		}

		list.remove(0);
		return list;
	}
}
